package a01aa01studios.hotball;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.Random;

import a01aa01studios.hotballq4.R;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * In this class, you can start the game, if you are not alone in the group.
 * You can leave the group by using buttonLeaveGroup
 */
public class GroupActivity extends AppCompatActivity {
    private Button buttonLeaveGroup;
    private Button buttonStartGame;

    /**
     * automatically generated
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "onCreate");

        //there might be an error, that the group is none
        if ("none".equals(Utilities.getGroupName())) {
            Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "Group is none: switch back to MainActivity");
            startActivity(new Intent(GroupActivity.this, MainActivity.class));
        }

        buttonLeaveGroup = findViewById(R.id.buttonLeaveGame);
        buttonStartGame = findViewById(R.id.buttonStartGame);
        TextView textViewGroupName = findViewById(R.id.textViewGroupName);
        textViewGroupName.setText(Utilities.getGroupName());

        buttonLeaveGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLeaveGroup.setEnabled(false);
                leaveGroup();
            }
        });
        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStartGame.setEnabled(false);
                startGame();
            }
        });


        Utilities.getRefUser().child("group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Utilities.getRefGroup().child("isGameStarted").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "snapshot is null");
                        } else {
                            String isGameStarted = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                            if ("true".equals(isGameStarted)) {
                                Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "game started");
                                Utilities.health = Constants.START_HEALTH;
                                Utilities.getRefGroup().child("isGameStarted").removeEventListener(this);
                                startActivity(new Intent(GroupActivity.this, GameActivity.class));
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Utilities.handleConnectionError(GroupActivity.this);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GroupActivity.this);
            }
        });

    }

    /**
     * when called, the player leaves the group. The username gets removed from the group database
     * and the group name in the user database gets replaced with none
     */
    private void leaveGroup() {
        Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "leaveGroup");
        // get reference and user id
        Utilities.getRefUser().child("group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // get reference and user id
                final String userID = Utilities.getCurrentUserID();
                // if last member delete isGameTrue
                Utilities.getRefGroup().child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int size = (int) dataSnapshot.getChildrenCount();
                        if (size == 1) {
                            // remove group
                            Utilities.getRefGroup().setValue(null);
                            // set users group to none
                            Utilities.setGroupName("none");
                        } else {
                            // set users group to none
                            Utilities.setGroupName("none");
                            // remove user form group
                            Utilities.getRefGroup().child("members").child(userID).setValue(null);
                        }
                        // start activity
                        startActivity(new Intent(GroupActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Utilities.handleConnectionError(GroupActivity.this);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GroupActivity.this);
            }
        });
    }

    /**
     * starts the game and lets every player know
     * by setting the value gameStarted in the group database to true
     */
    private void startGame() {
        Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "startGame");
        final String userID = Utilities.getCurrentUserID();
        Utilities.getRefUser().child("group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //choose first player to have ball
                Utilities.getRefGroup().child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //get array of member uids
                        String[] members = new String[(int) dataSnapshot.getChildrenCount()];
                        int index = 0;
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            if (!userID.equals(snap.getKey())) {
                                members[index] = snap.getKey();
                                index++;
                            }
                        }
                        if (members.length <= 1) {
                            Utilities.toast("You are alone.", GroupActivity.this);
                            buttonStartGame.setEnabled(true);
                            return;
                        }
                        Utilities.getRefGroup().child("isGameStarted").setValue(true);
                        //set health to start health
                        Utilities.health = Constants.START_HEALTH;
                        //choose random userID
                        int rnd = new Random().nextInt(members.length - 1);
                        String chooseUID = members[rnd];
                        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "chooseUID: " + chooseUID);
                        //set value in db: group/whoHasBall to choose player uid
                        Utilities.getRefGroup().child("whoHasBall").setValue(chooseUID);
                        //set value in database: group/timeBallShoot to current time in millies
                        Utilities.getRefGroup().child("timeBallShoot").setValue(System.currentTimeMillis());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Utilities.handleConnectionError(GroupActivity.this);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GroupActivity.this);
            }
        });
    }
}