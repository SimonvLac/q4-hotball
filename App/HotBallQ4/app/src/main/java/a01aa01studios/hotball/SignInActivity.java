package a01aa01studios.hotball;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.Objects;

import a01aa01studios.hotballq4.R;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * User can create account with email and password.
 * He can sign into an existing account.
 * The class adds User with UserID and Name to Database.
 */
public class SignInActivity extends AppCompatActivity {
    // firebase
    private FirebaseAuth mAuth;
    // ui
    private Button buttonSignIn;
    private Button buttonCreateAccount;
    private EditText editTextPersonName;
    private EditText editTextPassword;

    /**
     * automatically generated
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "onCreate");

        // set views
        buttonSignIn = findViewById(R.id.buttonSignIn);
        buttonCreateAccount = findViewById(R.id.buttonCreateAccount);
        editTextPersonName = findViewById(R.id.editTextPersonName);
        editTextPassword = findViewById(R.id.editTextPassword);

        // ui listener
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSignIn.setEnabled(false);
                signIn();
            }
        });
        buttonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCreateAccount.setEnabled(false);
                createAccount();
            }
        });
        // firebase
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * automatically generated
     */
    @Override
    protected void onStart() {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "onStart");
        super.onStart();
        // checks if the user might already is signed in, then this activity can me skipped
        if (isSignedIn()) {
            signedIn();
        }

    }

    /**
     * gets email and password form editText
     * uses email and password sign in form firebase
     * if sign in was successful, calls the method signedIn()
     */
    private void signIn() {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "signIn");
        // get input from edit text
        String email = editTextPersonName.getText().toString();
        String password = editTextPassword.getText().toString();
        // sign in
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SignIn", "signInWithEmail:success");
                            signedIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SignIn", "signInUserWithEmail:failure", task.getException());
                            String error = Objects.requireNonNull(task.getException()).getMessage();
                            Toast.makeText(SignInActivity.this, error,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * gets email and password form editText
     * uses email and password create account form firebase
     * if sign in was successful, calls the method signedIn()
     */
    private void createAccount() {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "createAccount");
        // get input from edit text
        final String email = editTextPersonName.getText().toString();
        String password = editTextPassword.getText().toString();
        // create account
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SignIn", "createUserWithEmail:success");
                            addUserToDatabase(email);
                            signedIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SignIn", "createUserWithEmail:failure", task.getException());
                            String error = Objects.requireNonNull(task.getException()).getMessage();
                            Toast.makeText(SignInActivity.this, error,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * this method gets called, when the user was successfully signed in
     * it starts the MainActivity
     */
    private void signedIn() {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "signedIn");
        startActivity(new Intent(SignInActivity.this, MainActivity.class));
    }

    /**
     * @return checks if the user is signed in in firebase
     */
    private boolean isSignedIn() {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "isSignedIn()");
        FirebaseUser currentUser = mAuth.getCurrentUser();
        return currentUser != null;
    }

    /**
     * @param email this method adds a new user with its user id from auth to the database
     */
    private void addUserToDatabase(String email) {
        Log.d(Constants.DEBUG_TAG + SignInActivity.class.getSimpleName(), "addUserToDatabase");
        FirebaseUser user = mAuth.getCurrentUser();
        String userName = email.split("@")[0];

        // add display name to the firebase user
        UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(userName).build();
        if (user != null) {
            user.updateProfile(profileChangeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful())
                        Log.d(SignInActivity.class.getSimpleName(), "User Profile updated");
                }
            });
        }

        // adds user data to
        Utilities.setGroupName("none");
        Utilities.getRefUser().child("name").setValue(userName);
    }

}
