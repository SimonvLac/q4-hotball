package a01aa01studios.hotball;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import a01aa01studios.hotballq4.R;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * In this class the User can create a new group.
 * He can join an existing group.
 * The class checks if groupName is valid.
 */
public class MainActivity extends AppCompatActivity {
    private Button buttonCreateGroup;
    private Button buttonJoinGroup;
    private Button buttonConfirmGroupName;
    private EditText etConfirmGroupName;

    private Boolean isCreateGroup; // if the user wants to create a group this is true

    /**
     * automatically generated
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "onCreate");


        buttonCreateGroup = findViewById(R.id.buttonCreateGroup);
        buttonJoinGroup = findViewById(R.id.buttonJoinGroup);
        buttonConfirmGroupName = findViewById(R.id.buttonConfirmGroupName);
        final Button buttonLogOut = findViewById(R.id.buttonLogOut);
        etConfirmGroupName = findViewById(R.id.etConfirmGroupName);

        buttonCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCreateGroup.setEnabled(false);
                isCreateGroup = true;
                confirmGroupName();
            }
        });
        buttonJoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonJoinGroup.setEnabled(false);
                isCreateGroup = false;
                confirmGroupName();
            }
        });
        buttonConfirmGroupName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //test if groups
                boolean isTested = true;
                String groupName;
                if (etConfirmGroupName.getText() == null)
                    groupName = "";
                else
                    groupName = etConfirmGroupName.getText().toString();
                if ("none".equals(groupName))
                    isTested = false;
                if ("".equals(groupName))
                    isTested = false;
                if (isTested) {
                    if (isCreateGroup) {
                        createGroup(groupName);
                        buttonConfirmGroupName.setEnabled(false);
                    } else {
                        joinGroup(groupName);
                        buttonConfirmGroupName.setEnabled(false);
                    }
                } else {
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "Name invalid");
                    Toast.makeText(MainActivity.this, "Name invalid", Toast.LENGTH_SHORT).show();
                    buttonConfirmGroupName.setEnabled(true);
                }
            }
        });
        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLogOut.setEnabled(false);
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "start SignInActivity");
            }
        });

        if (!"none".equals(Utilities.getGroupName())) {
            startActivity(new Intent(MainActivity.this, GroupActivity.class));
        }
    }

    /**
     * changes UI so user can create Group name by making buttons visible/invisible
     */
    private void confirmGroupName() {
        Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "confirmGroupName");
        buttonCreateGroup.setVisibility(View.INVISIBLE);
        buttonJoinGroup.setVisibility(View.INVISIBLE);
        buttonConfirmGroupName.setVisibility(View.VISIBLE);
        etConfirmGroupName.setVisibility(View.VISIBLE);
    }

    /**
     * creates group in group database
     * adds group to this user in user database with the value admin
     * starts the GroupActivity
     */
    private void createGroup(final String groupName) {
        Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "createGroup");
        final String UID = Utilities.getCurrentUserID();
        final DatabaseReference databaseReference = Utilities.getDatabaseRef();

        //checks for valid group name, creates the group
        databaseReference.child("group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean isNameTaken = false;
                if (dataSnapshot.getValue() != null && dataSnapshot.getValue().toString().contains(groupName))
                    isNameTaken = true;
                if (isNameTaken) {
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "Name already taken");
                    Utilities.toast("Name already taken.", MainActivity.this);
                    buttonConfirmGroupName.setEnabled(true);
                } else {
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "GroupName is available");
                    //add user to the group
                    Utilities.getDatabaseRef().child("group").child(groupName).child("members").child(UID).setValue(true);
                    //change group name in user
                    Utilities.setGroupName(groupName);
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "set group name: " + groupName);
                    //introduce isGameStarted
                    Utilities.getDatabaseRef().child("group").child(groupName).child("isGameStarted").setValue(false);
                    //change to group activity
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "start " + GroupActivity.class.getSimpleName());
                    startActivity(new Intent(MainActivity.this, GroupActivity.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(MainActivity.this);
            }
        });
    }

    /**
     * adds this user to the group in the group database with the value member
     * adds group to this user in user database
     * starts the GroupActivity
     */
    private void joinGroup(final String groupName) {
        Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "joinGroup");
        final String UID = Utilities.getCurrentUserID();
        final DatabaseReference databaseReference = Utilities.getDatabaseRef();

        //check if the group already exists, if not displays error message
        databaseReference.child("group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null && dataSnapshot.getValue().toString().contains(groupName))
                    Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "GroupName does not exists, user created group");

                Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "GroupName exists, user can join");
                //add user to the group
                Utilities.getDatabaseRef().child("group").child(groupName).child("members").child(UID).setValue(true);
                //change group name in user
                Utilities.setGroupName(groupName);
                //change to group activity
                Log.d(Constants.DEBUG_TAG + MainActivity.class.getSimpleName(), "start " + GroupActivity.class.getSimpleName());
                startActivity(new Intent(MainActivity.this, GroupActivity.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(MainActivity.this);
            }
        });
    }
}
