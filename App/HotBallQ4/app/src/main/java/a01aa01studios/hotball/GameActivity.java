package a01aa01studios.hotball;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.Random;

import a01aa01studios.hotballq4.R;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * In this class you can shoot the ball to an opponent.
 * You can leave the game.
 * You lose health per second.
 * When health is 0 GameOver Screen appears.
 * Last user with ball wins.
 */
public class GameActivity extends AppCompatActivity {
    private ImageButton buttonShootBall;
    private Button buttonLeaveGame;
    private TextView textViewWhoHasBall;
    private TextView textViewGameOver;
    private Button buttonToMain;

    private ValueEventListener whoHasBallListener;
    private CountDownTimer healthTimer;

    /**
     * automatically generated
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "onCreate");
        //define buttons
        buttonLeaveGame = findViewById(R.id.buttonLeaveGame);
        buttonShootBall = findViewById(R.id.buttonShootBall);
        textViewWhoHasBall = findViewById(R.id.textViewWhoHasBall);
        textViewGameOver = findViewById(R.id.textViewGameOver);
        buttonToMain = findViewById(R.id.buttonToMain);

        textViewWhoHasBall.setVisibility(View.VISIBLE);
        //set on click listeners
        buttonShootBall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonShootBall.setEnabled(false);
                buttonShootBall.setVisibility(View.INVISIBLE);
                shootBall(true);
            }
        });
        buttonLeaveGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLeaveGame.setEnabled(false);
                leaveGroupInDatabase();
            }
        });
        buttonToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLeaveGame.setEnabled(false);
                startActivity(new Intent(GameActivity.this, MainActivity.class));
            }
        });
    }

    /**
     * automatically generated
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "onStart");
        Utilities.health = Constants.START_HEALTH;
        //who has ball
        whoHasBallListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Listener that runs all the time to listen for changes in db "group/whoHasBall"if(dataSnapshot.getValue() != null) {
                final String whoHasBall = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "user with ball: " + whoHasBall);
                //if the value is the current users id
                boolean iHaveBall = Utilities.getCurrentUserID().equals(whoHasBall);
                if (iHaveBall) {
                    Utilities.getRefGroup().child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            int numMembers = (int) dataSnapshot.getChildrenCount();
                            Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "groupSize " + numMembers);
                            final boolean iWin = (numMembers <= 1);
                            if (iWin) {
                                leaveGroupInDatabase();
                            } else {
                                hasBall();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Utilities.handleConnectionError(GameActivity.this);
                        }
                    });
                } else {
                    // get users name, not display id
                    Utilities.getDatabaseRef().child("user").child(whoHasBall).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String username = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                            textViewWhoHasBall.setText(username);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Utilities.handleConnectionError(GameActivity.this);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GameActivity.this);
            }
        };
        //start event listener
        Utilities.getRefGroup().child("whoHasBall").addValueEventListener(whoHasBallListener);
    }

    /**
     * is called, when the player gets the ball.
     * get the time when the ball was sent from the database
     * compares it with the current time
     * subtracts the difference from Utilities.health
     */
    private void healthloss() {
        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "healthloss");
        //get groupName
        Utilities.getRefGroup().child("timeBallShoot").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long timeBallShoot = Long.parseLong(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                // get current time milliseconds
                long currentTime = System.currentTimeMillis();
                // calculate difference
                int timeDifference = Integer.parseInt("" + (currentTime - timeBallShoot));
                Utilities.health = Utilities.health - timeDifference;
                boolean iAmAlive = Utilities.health > 0;
                if (iAmAlive) {
                    healthTimer = new CountDownTimer(Utilities.health, 1000) {
                        public void onTick(long millisUntilFinished) {
                            TextView textViewHealth = findViewById(R.id.textViewHealth);
                            textViewHealth.setText(String.valueOf(millisUntilFinished / 1000));
                            Utilities.health = (int) millisUntilFinished;
                        }

                        public void onFinish() {
                            leaveGroupInDatabase();
                        }
                    };
                    healthTimer.start();
                } else {
                    leaveGroupInDatabase();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GameActivity.this);
            }
        });
    }

    /**
     * moves the ball to a random player
     * by: getting a list of all members in a group
     * choosing a random member
     * changing the value whoHasBall in the database to that user id
     * updating the value timeBallShoot to the current time
     */
    private void shootBall(final boolean iWin) {
        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "shootBall" + iWin);
        //get groupName
        Utilities.getRefGroup().child("members").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //get array of member user ids
                Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "children" + dataSnapshot.getChildrenCount());
                String[] members = new String[(int) dataSnapshot.getChildrenCount()];
                int index = 0;
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (!Utilities.getCurrentUserID().equals(snap.getKey())) {
                        members[index] = snap.getKey();
                        index++;
                    }
                }
                Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "member.length: " + members.length);
                if (members.length == 1) {
                    leaveGroupInDatabase();
                } else {
                    //choose random userID
                    int rnd = new Random().nextInt(members.length - 1);
                    String chooseUID = members[rnd];
                    Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "chooseUID: " + chooseUID);
                    //cancel ongoing health timer
                    try {
                        healthTimer.cancel();
                    } catch (Exception e) {
                        //timer might not have been initialized yet
                        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "ERROR:" + e.getMessage());
                    }
                    // wait for 1/2s
                    try {
                        Thread.sleep(Constants.flightTime);
                    } catch (InterruptedException e) {
                        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "ERROR:" + e.getMessage());
                    }
                    //set value in db: group/whoHasBall to choose player uid
                    Utilities.getRefGroup().child("whoHasBall").setValue(chooseUID);
                    Utilities.getRefGroup().child("timeBallShoot").setValue(System.currentTimeMillis());
                    //make who has ball visible
                    textViewWhoHasBall.setVisibility(View.VISIBLE);
                    if (!iWin) {
                        leaveGroupInDatabase2(false);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GameActivity.this);
            }
        });
    }

    /**
     * removes event listener, that update ui
     * sets ui game buttons invisible and game over buttons visible
     * calls leaveGroup
     *
     * @param iWin decides if win text or lose text should be displayed
     */
    private void gameOverScreen(boolean iWin, String groupName) {
        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "gameOver: " + iWin);
        //cancel listeners
        Utilities.getDatabaseRef().child("group").child(groupName).child("whoHasBall").removeEventListener(whoHasBallListener);
        //hide all views
        buttonShootBall.setVisibility(View.INVISIBLE);
        TextView textViewHealth = findViewById(R.id.textViewHealth);
        textViewHealth.setVisibility(View.INVISIBLE);
        textViewWhoHasBall.setVisibility(View.INVISIBLE);
        buttonLeaveGame.setVisibility(View.INVISIBLE);
        buttonToMain.setVisibility(View.VISIBLE);
        buttonToMain.setEnabled(true);
        //show text view game over
        if (iWin)
            textViewGameOver.setText("You Win!");
        else
            textViewGameOver.setText("You Lose.");
        textViewGameOver.setVisibility(View.VISIBLE);
    }

    /**
     * gets called, when the user receives the ball
     * makes buttons invisible/visible if User has ball
     * call healthloss
     */
    private void hasBall() {
        Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "hasBall");
        buttonShootBall.setEnabled(true);
        buttonShootBall.setVisibility(View.VISIBLE);
        textViewWhoHasBall.setVisibility(View.INVISIBLE);
        healthloss();
    }

    /**
     * removes group from database and switches to gameOverScreen
     * @param iWin if the player won or not -> gets send to gameOverScreen
     */
    private void leaveGroupInDatabase2(boolean iWin) {
        Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "leaveGroupInDatabase2 iWin:" + iWin);
        //leave database
        if (iWin) {
            Utilities.getRefGroup().setValue(null);
        } else {
            Utilities.getRefGroup().child("members").child(Utilities.getCurrentUserID()).setValue(null);
        }
        // set users group to none
        gameOverScreen(iWin, Utilities.getGroupName());
        Utilities.setGroupName("none");
    }

    /**
     * leaves the group in the data base
     * chooses if the ball should be shoot to another player
     * switches to gameOverScreen()
     */
    private void leaveGroupInDatabase() {
        Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "leaveGroupInDatabase");
        //leave Database
        Utilities.getRefGroup().child("members").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int numMembers = (int) dataSnapshot.getChildrenCount();
                Log.d(Constants.DEBUG_TAG + GroupActivity.class.getSimpleName(), "groupSize " + numMembers);
                final boolean iWin = (numMembers <= 1);
                //choose if ball shoot be shot
                if (iWin) {
                    leaveGroupInDatabase2(true);
                } else {
                    //check if has ball
                    Utilities.getRefGroup().child("whoHasBall").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //Listener that runs all the time to listen for changes in db "group/whoHasBall"if(dataSnapshot.getValue() != null) {
                            final String whoHasBall = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                            Log.d(Constants.DEBUG_TAG + GameActivity.class.getSimpleName(), "user with ball: " + whoHasBall);
                            boolean iHaveBall = Utilities.getCurrentUserID().equals(whoHasBall);
                            if (iHaveBall) {
                                shootBall(false);
                            } else {
                                leaveGroupInDatabase2(false);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Utilities.handleConnectionError(GameActivity.this);
                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Utilities.handleConnectionError(GameActivity.this);
            }
        });
    }
}
