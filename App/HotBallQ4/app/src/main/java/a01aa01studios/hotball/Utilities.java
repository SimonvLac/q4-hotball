package a01aa01studios.hotball;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

/**
 * This class contains methods which are used in multiple activities and static.
 */
class Utilities {
    /**
     * this is the health of the player (in milliseconds)
     * this is always correct
     */
    static int health;
    /**
     * this is the groupName of the group the user is in, this is always correct
     */
    private static String groupName = "none";

    /**
     * @return get for groupName
     */
    static String getGroupName() {
        return groupName;
    }

    /**setter for groupName
     * also updates database and sends log
     * @param name new groupName
     */
    static void setGroupName(String name) {
        Log.d(Constants.DEBUG_TAG + Utilities.class.getSimpleName(), "setGroupName: " + name);
        groupName = name;
        Utilities.getRefUser().child("group").setValue(groupName);
    }

    /**
     * @return firebase database, the root. Call .child on this
     */
    static DatabaseReference getDatabaseRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    /**
     * abbreviation for Group Reference
     */
    static DatabaseReference getRefGroup() {
        return getDatabaseRef().child("group").child(getGroupName());
    }

    /**
     * abbreviation for User Reference
     */
    static DatabaseReference getRefUser() {
        return getDatabaseRef().child("user").child(getCurrentUserID());
    }

    /**
     * @return signed in users userID, to use in database
     */
    static String getCurrentUserID() {
        return Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
    }

    /**
     * creates a toast displaying "Connection Error"
     *
     * @param context use activity.this
     */
    static void handleConnectionError(Context context) {
        Log.d(Constants.DEBUG_TAG + Utilities.class.getSimpleName(), "Connection Error");
        toast("Connection Error", context);
    }

    /**
     * creates Toast
     */
    static void toast(String string, Context context) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
    }
}
