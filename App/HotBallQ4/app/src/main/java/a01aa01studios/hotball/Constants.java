package a01aa01studios.hotball;

/**
 * This class contains all variables, that are used in more than one class and are final.
 */
class Constants {
    /**
     * console tag for debugging
     */
    static final String DEBUG_TAG = "HOTBALL_DEBUG ";
    /**
     * start health in milliseconds
     */
    final static int START_HEALTH = 60 * 1000;
    /**
     * time from shooting ball to receiving ball
     */
    static final long flightTime = 500; //milliseconds
}
